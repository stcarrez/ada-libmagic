MAKE_ARGS=

ifeq ($(BUILD),coverage)
MAKE_ARGS=-- -XMAGICADA_BUILD=coverage
endif

ifeq ($(BUILD),debug)
MAKE_ARGS=-- -XMAGICADA_BUILD=debug
endif

build:
	alr build $(MAKE_ARGS)

clean:
	alr clean
	rm -rf obj lib regtests/bin

test:
	cd regtests && alr build $(MAKE_ARGS)
	./regtests/bin/magic-harness
