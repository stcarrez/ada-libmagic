-- --------------------------------------------------------------------
--  magic-manager-tests -- Tests for magic manager
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with Util.Test_Caller;
package body Magic.Manager.Tests is

   package Caller is new Util.Test_Caller (Test, "Magic.Manager");

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite) is
   begin
      Caller.Add_Test (Suite, "Test Magic.Initialize",
                       Test_Initialize'Access);
      Caller.Add_Test (Suite, "Test Magic.Is_Initialized",
                       Test_Is_Initialized'Access);
      Caller.Add_Test (Suite, "Test Magic.Identify (String)",
                       Test_Identify_File'Access);
      Caller.Add_Test (Suite, "Test Magic.Identify (Stream_Element_Array)",
                       Test_Identify_Buffer'Access);
      Caller.Add_Test (Suite, "Test Magic.Error",
                       Test_Error'Access);
   end Add_Tests;

   procedure Test_Is_Initialized (T : in out Test) is
      Mgr  : Magic_Manager;
   begin
      T.Assert (not Mgr.Is_Initialized, "Not initialized failed");
      Mgr.Initialize (Magic.MAGIC_MIME, Magic.DEFAULT_PATH);
      T.Assert (Mgr.Is_Initialized, "Not initialized failed");
   end Test_Is_Initialized;

   procedure Test_Initialize (T : in out Test) is
      Mgr  : Magic_Manager;
      Path : constant String
        := Util.Tests.Get_Path ("alire.toml");
   begin
      Mgr.Initialize (Magic.MAGIC_MIME, Magic.DEFAULT_PATH);
      T.Assert (Mgr.Is_Initialized, "Not initialized");

      Mgr.Initialize (Magic.MAGIC_NONE, Magic.DEFAULT_PATH);
      T.Assert (Mgr.Is_Initialized, "Not initialized");

      Util.Tests.Assert_Equals (T, "ASCII text",
                                Mgr.Identify (Path), "Cannot identify file");

      Util.Tests.Assert_Equals (T, 0, Mgr.Last_Errno, "Invalid errno");
      Util.Tests.Assert_Equals (T, "", Mgr.Last_Error, "Invalid error");
   end Test_Initialize;

   procedure Test_Error (T : in out Test) is
      Mgr  : Magic_Manager;
      Path : constant String
        := Util.Tests.Get_Path ("alire.toml");
   begin
      begin
         Mgr.Initialize (-1, "a-missing-magic-file.bin");
         T.Fail ("No exception raised");

      exception
         when Error =>
            null;
      end;

      Util.Tests.Assert_Equals (T, 0, Mgr.Last_Errno, "Invalid errno");
      Util.Tests.Assert_Equals (T, "could not find any valid magic files!",
                                Mgr.Last_Error, "Invalid error");

      begin
         Util.Tests.Assert_Equals (T, "text/plain; charset=us-ascii",
                                   Mgr.Identify (Path), "Cannot identify file");
      exception
         when Error =>
            null;
      end;

      Util.Tests.Assert_Equals (T, 0, Mgr.Last_Errno, "Invalid errno");
      Util.Tests.Assert_Equals (T, "could not find any valid magic files!",
                                Mgr.Last_Error, "Invalid error");

      Mgr.Initialize (Magic.MAGIC_NONE, Magic.DEFAULT_PATH);
      T.Assert (Mgr.Is_Initialized, "Not initialized");

      begin
         Mgr.Initialize (-1, "a-missing-magic-file.bin");
         T.Fail ("No exception raised");

      exception
         when Error =>
            null;
      end;
      Util.Tests.Assert_Equals (T, 0, Mgr.Last_Errno, "Invalid errno");
      Util.Tests.Assert_Equals (T, "could not find any valid magic files!",
                                Mgr.Last_Error, "Invalid error");

   end Test_Error;

   procedure Test_Identify_File (T : in out Test) is
      Mgr  : Magic_Manager;
      Path : constant String
        := Util.Tests.Get_Path ("alire.toml");
   begin
      Mgr.Initialize (Magic.MAGIC_MIME, Magic.DEFAULT_PATH);
      T.Assert (Mgr.Is_Initialized, "Not initialized");

      Util.Tests.Assert_Equals (T, "text/plain; charset=us-ascii",
                                Mgr.Identify (Path), "Cannot identify file");

      Util.Tests.Assert_Equals (T, 0, Mgr.Last_Errno, "Invalid errno");
      Util.Tests.Assert_Equals (T, "", Mgr.Last_Error, "Invalid error");
   end Test_Identify_File;

   --  ------------------------------
   --  Test reading a file with multiline comments.
   --  ------------------------------
   procedure Test_Identify_Buffer (T : in out Test) is
      Mgr  : Magic_Manager;
      Content : String := "some text content";
      Data : Ada.Streams.Stream_Element_Array (1 .. Content'Length);
      for Data'Address use Content'Address;
   begin
      Mgr.Initialize (Magic.MAGIC_MIME, Magic.DEFAULT_PATH);
      Util.Tests.Assert_Equals (T, "text/plain; charset=us-ascii",
                                Mgr.Identify (Data), "Cannot identify buffer");
      Util.Tests.Assert_Equals (T, 0, Mgr.Last_Errno, "Invalid errno");
      Util.Tests.Assert_Equals (T, "", Mgr.Last_Error, "Invalid error");

      for I in Content'Range loop
         Content (I) := Character'Val (255);
      end loop;
      Util.Tests.Assert_Equals (T, "text/plain; charset=iso-8859-1",
                                Mgr.Identify (Data), "Cannot identify buffer");
      Util.Tests.Assert_Equals (T, 0, Mgr.Last_Errno, "Invalid errno");
      Util.Tests.Assert_Equals (T, "", Mgr.Last_Error, "Invalid error");

      Content (1) := Character'Val (16#00#);
      Content (2) := Character'Val (16#10#);
      Content (3) := Character'Val (16#05#);
      Content (4) := Character'Val (16#54#);
      Util.Tests.Assert_Equals (T, "application/octet-stream; charset=binary",
                                Mgr.Identify (Data), "Cannot identify buffer");
      Util.Tests.Assert_Equals (T, 0, Mgr.Last_Errno, "Invalid errno");
      Util.Tests.Assert_Equals (T, "", Mgr.Last_Error, "Invalid error");

      Mgr.Initialize (Magic.MAGIC_NONE, Magic.DEFAULT_PATH);
      Util.Tests.Assert_Equals (T, "data",
                                Mgr.Identify (Data), "Cannot identify buffer");
      Util.Tests.Assert_Equals (T, 0, Mgr.Last_Errno, "Invalid errno");
      Util.Tests.Assert_Equals (T, "", Mgr.Last_Error, "Invalid error");

   end Test_Identify_Buffer;

end Magic.Manager.Tests;
