-- --------------------------------------------------------------------
--  magic-manager-tests -- Tests for magic manager
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with Util.Tests;
package Magic.Manager.Tests is

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite);

   type Test is new Util.Tests.Test with null record;

   procedure Test_Is_Initialized (T : in out Test);

   procedure Test_Identify_File (T : in out Test);

   procedure Test_Identify_Buffer (T : in out Test);

   procedure Test_Initialize (T : in out Test);

   procedure Test_Error (T : in out Test);

end Magic.Manager.Tests;
