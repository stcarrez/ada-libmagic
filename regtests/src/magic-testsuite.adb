-- --------------------------------------------------------------------
--  magic-testsuite -- Magic testsuite
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with Magic.Manager.Tests;
package body Magic.Testsuite is

   Tests : aliased Util.Tests.Test_Suite;

   function Suite return Util.Tests.Access_Test_Suite is
   begin
      Magic.Manager.Tests.Add_Tests (Tests'Access);
      return Tests'Access;
   end Suite;

end Magic.Testsuite;
