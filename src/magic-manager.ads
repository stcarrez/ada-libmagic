-- --------------------------------------------------------------------
--  magic-manager -- magic manager to help in using the libmagic library
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with Ada.Finalization;
with Ada.Streams;
with Ada.Directories;

--  === Magic Manager ==
--  To simplify and help in the use of the magic Ada library, the
--  `Magic.Manager` package encapsulates the management of the libmagic library
--  and provides high level simple operations to hide the details of interacting
--  with the C library.  The package defines the `Magic_Manager` tagged record
--  that takes care of the C library interaction and cleaning of the libmagic
--  library once the manager is no longer required.  To use it, declare the
--  instance and initialize it with the `Initialize` procedure:
--
--     with Magic.Manager;
--     ...
--        Mgr : Magic.Manager.Magic_Manager;
--        ...
--           Mgr.Initialize (Magic.MAGIC_MIME, Magic.DEFAULT_PATH);
--
--  The first parameter defines a set of flags represented by the `Magic.Flags`
--  type to control various options of the libmagic library.   The second
--  parameter indicates the default path to the magic file (see magic (5)).
--  Once configured, the `Identify` functions can be used to identify a content.
--  For the first form, a path of the file is given:
--
--     Mime : constant String := Mgr.Identify ("file.ads");
--
--  With the second form, a `Stream_Element_Array` with the content to identify
--  is given to the function.
package Magic.Manager is

   package AF renames Ada.Finalization;

   Error : exception;

   type Magic_Manager is limited new AF.Limited_Controlled with private;

   --  Initialize the magic manager and prepare the libmagic library.
   procedure Initialize (Manager : in out Magic_Manager;
                         Config  : in Flags;
                         Path    : in String);

   --  Check whether the manager and the libmagic library is initialized.
   function Is_Initialized (Manager : in Magic_Manager) return Boolean;

   --  Identify the content of the buffer by using the libmagic library.
   function Identify (Manager : in Magic_Manager;
                      Data    : in Ada.Streams.Stream_Element_Array)
                      return String with
     Pre => Manager.Is_Initialized and then Data'Length > 0;

   --  Identify the content of the file.
   function Identify (Manager : in Magic_Manager;
                      Path    : in String) return String with
     Pre => Manager.Is_Initialized and then Ada.Directories.Exists (Path);

   --  Return the last error.
   function Last_Error (Manager : in Magic_Manager) return String;
   function Last_Errno (Manager : in Magic_Manager) return Integer;

   overriding
   procedure Finalize (Manager : in out Magic_Manager);

private

   type Magic_Manager is limited new AF.Limited_Controlled with record
      Magic_Cookie : Magic_t;
   end record;

end Magic.Manager;
