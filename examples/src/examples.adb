with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Directories;
with Magic.Manager;
procedure Examples is
   package AC renames Ada.Command_Line;

   Mgr : Magic.Manager.Magic_Manager;
begin
   Mgr.Initialize (Magic.MAGIC_MIME, Magic.DEFAULT_PATH);
   for I in 1 .. AC.Argument_Count loop
      declare
         Path : constant String := AC.Argument (I);
      begin
         if Ada.Directories.Exists (Path) then
            Ada.Text_IO.Put (Path);
            Ada.Text_IO.Put (": ");
            Ada.Text_IO.Put_Line (Mgr.Identify (Path));
         else
            Ada.Text_IO.Put (Path);
            Ada.Text_IO.Put_Line (": is not a file");
         end if;
      end;
   end loop;
end Examples;
